using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prisma.Api.Databases.Contexts;
using Prisma.Api.Models.Competidores;

namespace Prisma.Api.Areas.v1_0.Controllers
{
    [Route("api/v1.0/[controller]")]
    [ApiController]
    public class CompetidoresController : ControllerBase
    {
        private readonly PrismaDbContext _context;

        public CompetidoresController(PrismaDbContext context)
        {
            _context = context;
        }

        // GET: api/Competidores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Competidor>>> GetCompetidores(String filter)
        {
            if(!String.IsNullOrEmpty(filter))
            {
                return await _context.Competidores.Where(c => c.Nombre.Contains(filter) || c.Codigo.Contains(filter) || c.Calle.Contains(filter)).ToListAsync();
            }
            return await _context.Competidores.ToListAsync();
        }

        // GET: api/Competidores/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Competidor>> GetCompetidor(long id)
        {
            var competidor = await _context.Competidores.FindAsync(id);

            if (competidor == null)
            {
                return NotFound();
            }

            return competidor;
        }

        // PUT: api/Competidores/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompetidor(long id, Competidor competidor)
        {
            if (id != competidor.Id)
            {
                return BadRequest();
            }

            _context.Entry(competidor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompetidorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Competidores
        [HttpPost]
        public async Task<ActionResult<Competidor>> PostCompetidor(Competidor competidor)
        {
            _context.Competidores.Add(competidor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompetidor", new { id = competidor.Id }, competidor);
        }

        // DELETE: api/Competidores/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Competidor>> DeleteCompetidor(long id)
        {
            var competidor = await _context.Competidores.FindAsync(id);
            if (competidor == null)
            {
                return NotFound();
            }

            _context.Competidores.Remove(competidor);
            await _context.SaveChangesAsync();

            return competidor;
        }

        private bool CompetidorExists(long id)
        {
            return _context.Competidores.Any(e => e.Id == id);
        }
    }
}
