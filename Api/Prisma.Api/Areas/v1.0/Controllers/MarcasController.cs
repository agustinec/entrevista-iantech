using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Prisma.Api.Databases.Contexts;
using Prisma.Api.Models.Competidores;

namespace Prisma.Api.Areas.v1_0.Controllers
{
    [Route("api/v1.0/[controller]")]
    [ApiController]
    public class MarcasController : ControllerBase
    {
        private readonly PrismaDbContext _context;

        public MarcasController(PrismaDbContext context)
        {
            _context = context;
        }

        // GET: api/Marcas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Marca>>> GetMarcas(String filter)
        {
             if(!String.IsNullOrEmpty(filter))
            {
                return await _context.Marcas.Where(c => c.Nombre.Contains(filter) || c.Codigo.Contains(filter)).ToListAsync();
            }
            return await _context.Marcas.ToListAsync();
        }

        // GET: api/Marcas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Marca>> GetMarca(long id)
        {
            var marca = await _context.Marcas.FindAsync(id);

            if (marca == null)
            {
                return NotFound();
            }

            return marca;
        }

        // PUT: api/Marcas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMarca(long id, Marca marca)
        {
            if (id != marca.Id)
            {
                return BadRequest();
            }

            _context.Entry(marca).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MarcaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Marcas
        [HttpPost]
        public async Task<ActionResult<Marca>> PostMarca(Marca marca)
        {
            _context.Marcas.Add(marca);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMarca", new { id = marca.Id }, marca);
        }

        // DELETE: api/Marcas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Marca>> DeleteMarca(long id)
        {
            var marca = await _context.Marcas.FindAsync(id);
            if (marca == null)
            {
                return NotFound();
            }

            
            if(_context.Competidores.Any(e => e.MarcaId == id))
            {
                var result = new ModelStateDictionary();
                result.AddModelError("Conflict", "No se puede eliminar la Marca porque contiene Competidores asociados.");
                return  Conflict(result);
            }

            
            _context.Marcas.Remove(marca);
            await _context.SaveChangesAsync();

            return marca;
        }

        private bool MarcaExists(long id)
        {
            return _context.Marcas.Any(e => e.Id == id);
        }
    }
}
