using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Prisma.Api.Databases.Contexts;
using Prisma.Api.Models.Competidores;

namespace Prisma.Api.Areas.v1_0.Controllers
{
    [Route("api/v1.0/[controller]")]
    [ApiController]
    public class ZonaDePreciosController : ControllerBase
    {
        private readonly PrismaDbContext _context;

        public ZonaDePreciosController(PrismaDbContext context)
        {
            _context = context;
        }

        // GET: api/ZonaDePrecios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ZonaDePrecio>>> GetZonaDePrecios(String filter)
        {
            if(!String.IsNullOrEmpty(filter))
            {
                return await _context.ZonaDePrecios.Where(c => c.Nombre.Contains(filter) || c.Codigo.Contains(filter)).ToListAsync();
            }
            return await _context.ZonaDePrecios.ToListAsync();
        }

        // GET: api/ZonaDePrecios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ZonaDePrecio>> GetZonaDePrecio(long id)
        {
            var zonaDePrecio = await _context.ZonaDePrecios.FindAsync(id);

            if (zonaDePrecio == null)
            {
                return NotFound();
            }

            return zonaDePrecio;
        }

        // PUT: api/ZonaDePrecios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutZonaDePrecio(long id, ZonaDePrecio zonaDePrecio)
        {
            if (id != zonaDePrecio.Id)
            {
                return BadRequest();
            }

            _context.Entry(zonaDePrecio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZonaDePrecioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ZonaDePrecios
        [HttpPost]
        public async Task<ActionResult<ZonaDePrecio>> PostZonaDePrecio(ZonaDePrecio zonaDePrecio)
        {
            _context.ZonaDePrecios.Add(zonaDePrecio);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetZonaDePrecio", new { id = zonaDePrecio.Id }, zonaDePrecio);
        }

        // DELETE: api/ZonaDePrecios/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ZonaDePrecio>> DeleteZonaDePrecio(long id)
        {
            var zonaDePrecio = await _context.ZonaDePrecios.FindAsync(id);
            if (zonaDePrecio == null)
            {
                return NotFound();
            }


            if(_context.Competidores.Any(e => e.MarcaId == id))
            {
                var result = new ModelStateDictionary();
                result.AddModelError("Conflict", "No se puede eliminar la Zona de Precio porque contiene Competidores asociados.");
                return  Conflict(result);
            }

            _context.ZonaDePrecios.Remove(zonaDePrecio);
            await _context.SaveChangesAsync();

            return zonaDePrecio;
        }

        private bool ZonaDePrecioExists(long id)
        {
            return _context.ZonaDePrecios.Any(e => e.Id == id);
        }
    }
}
