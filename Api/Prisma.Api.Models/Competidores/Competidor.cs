﻿using Prisma.Api.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prisma.Api.Models.Competidores
{
    /// <summary>
    /// Entidad Competidor.
    /// </summary>
    public class Competidor : BaseModel
    {
        public String Codigo { get; set; }
        public String Nombre { get; set; }
        public String Calle { get; set; }
        public Decimal Latitud { get; set; }
        public Decimal Longitud { get; set; }
        public bool Marcador { get; set; }
        public bool Observar { get; set; }


        public long MarcaId { get; set; }
        public Marca Marca { get; set; }


        public long ZonaDePrecioId { get; set; }
        public ZonaDePrecio ZonaDePrecio { get; set; }
    }
}
