﻿using Prisma.Api.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prisma.Api.Models.Competidores
{
    /// <summary>
    /// Entidad Zona de Precio.
    /// </summary>
    public class ZonaDePrecio : BaseModel
    {
        public String Codigo { get; set; }
        public String Nombre { get; set; }

        public IEnumerable<Competidor> Competidores { get; set; }
    }
}
