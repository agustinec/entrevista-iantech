﻿using System;

namespace Prisma.Api.Models.Base
{
    /// <summary>
    /// EntityBase es la clase base de todas las entidades que se almacenarán en la base de datos.
    /// </summary>
    public class BaseModel
    {
        public BaseModel()
        {
            // Se genera un Guid único para la entidad.
            this.Guid = System.Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// Id único de la entidad
        /// </summary>
        public long? Id { get; set; }


        /// <summary>
        /// Guid único de la entidad. 
        /// En caso de consumir la Api desde dispositivos que puedan utilizarse Offline,
        /// La generación de un Guid en el dispositivo cliente, permite la sincronización
        /// de datos sin duplicidad de datos.
        /// </summary>
        public string Guid { get; set; }
       

        /// <summary>
        /// Fecha y Hora UTC de la creación de la entidad.
        /// </summary>
        public DateTime? CreatedAt { get; set; }


        /// <summary>
        /// Fecha UTC de la última actualización de la entidad.
        /// </summary>
        public DateTime? UpdatedAt { get; set; }


        // <summary>
        // Fecha  UTC de eliminación de la entidad.
        // Se utiliza cuando se quiere almacenar los datos para evitar la eliminación 
        // en cascada que el usuario pueda y suma funcionalidades como "revertir" una
        // una operación por el usuario.
        // En esta muestra lo comento, ya que no son requeridas dichas funciones.
        // </summary>
        //[JsonIgnore]
        //public DateTime? DeletedAt { get; set; }

    }
}
