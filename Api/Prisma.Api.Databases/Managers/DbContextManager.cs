﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Prisma.Api.Databases.Contexts;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Prisma.Api.Databases.Managers
{
    public static class DbContextManager
    {
        public static string GetPrismaDbContextConnectionString(IConfiguration Configuration)
        {
            return Configuration.GetSection("Databases:PrismaDbConnectionString").Get<string>();

        }

        public static PrismaDbContext GetPrismaDbContext(String ConnectionString)
        {
            var builder = new DbContextOptionsBuilder<PrismaDbContext>();
            builder.UseSqlServer(ConnectionString,
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(PrismaDbContext).GetTypeInfo().Assembly.GetName().Name));

            return new PrismaDbContext(builder.Options);
        }

        public static PrismaDbContext GetPrismaDbContext(IConfiguration Configuration)
        {
            return GetPrismaDbContext(GetPrismaDbContextConnectionString(Configuration));
        }

        public static void MigratePrismaDbContext(IConfiguration Configuration)
        {
            using (var db = GetPrismaDbContext(Configuration))
            {
                db.Database.Migrate();
            }
        }
    }
}
