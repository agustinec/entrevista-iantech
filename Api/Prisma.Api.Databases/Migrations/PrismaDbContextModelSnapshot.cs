﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Prisma.Api.Databases.Contexts;

namespace Prisma.Api.Databases.Migrations
{
    [DbContext(typeof(PrismaDbContext))]
    partial class PrismaDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Prisma.Api.Models.Competidores.Competidor", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Calle");

                    b.Property<string>("Codigo")
                        .HasMaxLength(64);

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Guid");

                    b.Property<decimal>("Latitud");

                    b.Property<decimal>("Longitud");

                    b.Property<long>("MarcaId");

                    b.Property<bool>("Marcador");

                    b.Property<string>("Nombre");

                    b.Property<bool>("Observar");

                    b.Property<DateTime>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<long>("ZonaDePrecioId");

                    b.HasKey("Id");

                    b.HasIndex("Guid")
                        .IsUnique()
                        .HasFilter("[Guid] IS NOT NULL");

                    b.HasIndex("MarcaId");

                    b.HasIndex("ZonaDePrecioId");

                    b.ToTable("Competidores");
                });

            modelBuilder.Entity("Prisma.Api.Models.Competidores.Marca", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Codigo")
                        .HasMaxLength(64);

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Guid");

                    b.Property<string>("Nombre")
                        .HasMaxLength(250);

                    b.Property<DateTime>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.HasKey("Id");

                    b.HasIndex("Guid")
                        .IsUnique()
                        .HasFilter("[Guid] IS NOT NULL");

                    b.ToTable("Marcas");
                });

            modelBuilder.Entity("Prisma.Api.Models.Competidores.ZonaDePrecio", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Codigo")
                        .HasMaxLength(64);

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Guid");

                    b.Property<string>("Nombre")
                        .HasMaxLength(250);

                    b.Property<DateTime>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("GETDATE()");

                    b.HasKey("Id");

                    b.HasIndex("Guid")
                        .IsUnique()
                        .HasFilter("[Guid] IS NOT NULL");

                    b.ToTable("ZonaDePrecios");
                });

            modelBuilder.Entity("Prisma.Api.Models.Competidores.Competidor", b =>
                {
                    b.HasOne("Prisma.Api.Models.Competidores.Marca", "Marca")
                        .WithMany("Competidores")
                        .HasForeignKey("MarcaId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Prisma.Api.Models.Competidores.ZonaDePrecio", "ZonaDePrecio")
                        .WithMany("Competidores")
                        .HasForeignKey("ZonaDePrecioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
