﻿using Prisma.Api.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Prisma.Api.Databases.ModelConfigurations
{
    
    public static class BaseModelExtends
    {
        /// <summary>
        /// Aplica la configuración a los Modelos heredados de BaseModel
        /// </summary>
        public static void SetDefaultProperties<T_Entity>(this EntityTypeBuilder<T_Entity> builder, string TableName)
           where T_Entity : BaseModel
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id); 
            builder.HasIndex(e => e.Guid).IsUnique();

            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.CreatedAt).IsRequired(true).ValueGeneratedOnAdd().HasColumnType("datetime").HasDefaultValueSql("GETDATE()");
            builder.Property(e => e.UpdatedAt).IsRequired(true).ValueGeneratedOnAddOrUpdate().HasColumnType("datetime").HasDefaultValueSql("GETDATE()");
        }
    }
    public class BaseModelModelConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseModel
    {
        private string TableName { get; set; }
        public BaseModelModelConfiguration(String TableName)
        {
            this.TableName = TableName;
        }

        public void Configure(EntityTypeBuilder<T> builder)
        {
            builder.SetDefaultProperties(TableName);
        }

        internal void SetDefaultProperties(EntityTypeBuilder<T> builder, string tableName)
        {
            throw new NotImplementedException();
        }
    }
}
