﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Prisma.Api.Models.Competidores;
using System;

namespace Prisma.Api.Databases.ModelConfigurations
{
    public static class PrismaModelConfiguration
    {
        public class CompetidorModelConfiguration : IEntityTypeConfiguration<Competidor>
        {
            private string TableName { get; set; }

            public CompetidorModelConfiguration(String TableName)
            {
                this.TableName = TableName;
            }

            public void Configure(EntityTypeBuilder<Competidor> builder)
            {
                // Aplico las propiedades básicas (Ver Enti
                builder.SetDefaultProperties(TableName);

                builder.Property(e => e.Codigo).HasMaxLength(64);

                // Aplico la Relación con Marca
                builder.HasOne(bc => bc.Marca)
                   .WithMany(b => b.Competidores)
                   .HasForeignKey(bc => bc.MarcaId)
                   .OnDelete(DeleteBehavior.Restrict);

                // Aplico la relación con Zona de Precio
                builder.HasOne(bc => bc.ZonaDePrecio)
                   .WithMany(b => b.Competidores)
                   .HasForeignKey(bc => bc.ZonaDePrecioId)
                   .OnDelete(DeleteBehavior.Restrict);
            }
        }


        public class MarcaModelConfiguration : IEntityTypeConfiguration<Marca>
        {
            private string TableName { get; set; }

            public MarcaModelConfiguration(String TableName)
            {
                this.TableName = TableName;
            }

            public void Configure(EntityTypeBuilder<Marca> builder)
            {
                // Aplico las propiedades básicas (Ver Enti
                builder.SetDefaultProperties(TableName);

                builder.Property(e => e.Codigo).HasMaxLength(64);
                builder.Property(e => e.Nombre).HasMaxLength(250);

            }
        }

        public class ZonaDePrecioModelConfiguration : IEntityTypeConfiguration<ZonaDePrecio>
        {
            private string TableName { get; set; }

            public ZonaDePrecioModelConfiguration(String TableName)
            {
                this.TableName = TableName;
            }

            public void Configure(EntityTypeBuilder<ZonaDePrecio> builder)
            {
                // Aplico las propiedades básicas (Ver Enti
                builder.SetDefaultProperties(TableName);

                builder.Property(e => e.Codigo).HasMaxLength(64);
                builder.Property(e => e.Nombre).HasMaxLength(250);
            }
        }
    }
}
