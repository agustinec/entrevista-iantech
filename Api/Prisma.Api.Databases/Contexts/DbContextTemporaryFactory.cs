﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Prisma.Api.Databases.Contexts
{
    /// <summary>
    /// Esta clase solo se utiliza para crear en vista diseño las migraciones.
    /// </summary>
    public class TemporarPrestoredyDbContextFactory : IDesignTimeDbContextFactory<PrismaDbContext>
    {
        public PrismaDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PrismaDbContext>();
            builder.UseSqlServer("erver=localhost;",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(PrismaDbContext).GetTypeInfo().Assembly.GetName().Name));
            return new PrismaDbContext(builder.Options);
        }
    }
}
