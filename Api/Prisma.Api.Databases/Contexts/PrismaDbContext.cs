﻿using Microsoft.EntityFrameworkCore;
using Prisma.Api.Models.Competidores;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prisma.Api.Databases.Contexts
{
    public class PrismaDbContext : DbContext
    {
        public PrismaDbContext(DbContextOptions<PrismaDbContext> options) : base(options)
        {

        }

        public DbSet<Competidor> Competidores { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<ZonaDePrecio> ZonaDePrecios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new Prisma.Api.Databases.ModelConfigurations.PrismaModelConfiguration.CompetidorModelConfiguration("Competidores"));
            modelBuilder.ApplyConfiguration(new Prisma.Api.Databases.ModelConfigurations.PrismaModelConfiguration.MarcaModelConfiguration("Marcas"));
            modelBuilder.ApplyConfiguration(new Prisma.Api.Databases.ModelConfigurations.PrismaModelConfiguration.ZonaDePrecioModelConfiguration("ZonaDePrecios"));
        }

    }
}
