import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { CompetidoresService } from 'app/shared/services/competidores.service';

@Component({
    selector   : 'selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class CompetidoresSelectedBarComponent implements OnInit, OnDestroy
{
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    hasSelectedCompetidores: boolean;
    isIndeterminate: boolean;
    selectedCompetidores: string[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CompetidoresService} _competidoresService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _competidoresService: CompetidoresService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._competidoresService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedCompetidores => {
                this.selectedCompetidores = selectedCompetidores;
                setTimeout(() => {
                    this.hasSelectedCompetidores = selectedCompetidores.length > 0;
                    this.isIndeterminate = (selectedCompetidores.length !== this._competidoresService._data.length && selectedCompetidores.length > 0);
                }, 0);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Select all
     */
    selectAll(): void
    {
        this._competidoresService.selectItem();
    }

    /**
     * Deselect all
     */
    deselectAll(): void
    {
        this._competidoresService.deselectItems();
    }

    /**
     * Delete selected competidores
     */
    deleteSelectedCompetidores(): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected competidores?';

        this.confirmDialogRef.afterClosed()
            .subscribe(result => {
                if ( result )
                {
                    this._competidoresService.deleteSelectedItems();
                }
                this.confirmDialogRef = null;
            });
    }
}
