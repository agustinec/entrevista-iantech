import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataSource } from '@angular/cdk/collections';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { CompetidoresService } from 'app/shared/services/competidores.service';

import { CompetidoresCompetidorFormDialogComponent } from '../form/competidor-form.component';

@Component({
    selector     : 'competidores-competidor-list',
    templateUrl  : './competidor-list.component.html',
    styleUrls    : ['./competidor-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class CompetidoresCompetidorListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    competidores: any;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'Codigo', 'Nombre', 'Calle', 'Latitud', 'Longitud', '_controls'];
    selectedCompetidores: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CompetidoresService} _competidoresService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _competidoresService: CompetidoresService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._competidoresService);

        this._competidoresService.onChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(competidores => {
                this.competidores = competidores;
                this.checkboxes = {};
                competidores.map(competidor => {
                    this.checkboxes[competidor.id] = false;
                });
            });

        this._competidoresService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedCompetidores => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedCompetidores.includes(id);
                }
                this.selectedCompetidores = selectedCompetidores;
            });

      

        this._competidoresService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._competidoresService.deselectItems();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit competidor
     *
     * @param competidor
     */
    editCompetidor(competidor): void
    {
        this.dialogRef = this._matDialog.open(CompetidoresCompetidorFormDialogComponent, {
            panelClass: 'competidor-form-dialog',
            data      : {
                competidor: competidor,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        //this._competidoresService.updateItem(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        //this.deleteCompetidor(competidor);

                        break;
                }
            });
    }

    /**
     * Delete Competidor
     */
    deleteCompetidor(competidor): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._competidoresService.deleteItem(competidor);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param competidorId
     */
    onSelectedChange(competidorId): void
    {
        this._competidoresService.toggleSelectedItem(competidorId);
    }

    
}

export class FilesDataSource extends DataSource<any>
{
    /**
     * Constructor
     *
     * @param {CompetidoresService} _competidoresService
     */
    constructor(
        private _competidoresService: CompetidoresService
    )
    {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]>
    {
        return this._competidoresService.onChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void
    {
    }
}
