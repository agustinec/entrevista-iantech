export const locale = {
    lang: 'es',
    data: {
        'COMPETIDORES': {
            'TITTLE': 'Lista de Competidores',
            'CODIGO': 'Código',
            'NOMBRE': 'Nombre',
            'CALLE': 'Calle',
            'LATITUD': 'Latitud',
            'LONGITUD': 'Longitud',
            'MARCA': 'Marca',
            'ZONADEPRECIO': 'Zona de Precio',
            'MARCADOR': 'Marcar',
            'OBSERVAR': 'Observar',
            'SEARCH': 'Buscar competidor...'
        }
    }
};
