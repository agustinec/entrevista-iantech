export const locale = {
    lang: 'en',
    data: {
        'COMPETIDORES': {
            'TITTLE': 'Competitors List', 
            'CODIGO': 'Code',
            'NOMBRE': 'Name',
            'CALLE': 'Street',
            'LATITUD': 'Latitude',
            'LONGITUD': 'Longitude',
            'MARCA': 'Brand',
            'ZONADEPRECIO': 'Price Zone',
            'MARCADOR': 'Marker',
            'OBSERVAR': 'Observer',
            'SEARCH': 'Search competitor...'
        }
    }
};
