import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { CompetidoresComponent } from './competidores.component';
import { CompetidoresService } from 'app/shared/services/competidores.service';
import { CompetidoresCompetidorListComponent } from './list/competidor-list.component';
import { CompetidoresSelectedBarComponent } from './selected-bar/selected-bar.component';
import { CompetidoresCompetidorFormDialogComponent } from './form/competidor-form.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FuseSidebarModule, FuseConfirmDialogModule } from '@fuse/components';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MarcasService } from 'app/shared/services/marcas.service';
import { MatSelectModule } from '@angular/material/select';
import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';

const routes = [
    {
        path     : 'competidores',
        component: CompetidoresComponent,
        resolve  : {
            competidores: CompetidoresService
        }
    }
];

@NgModule({
    declarations: [
        CompetidoresComponent,
        CompetidoresCompetidorListComponent,
        CompetidoresSelectedBarComponent,
        CompetidoresCompetidorFormDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,
        
MatSelectModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule
    ],
    providers      : [
        CompetidoresService,
        MarcasService,
        ZonaDePreciosService
    ],
    exports     : [
        CompetidoresComponent
    ],
    entryComponents: [
        CompetidoresCompetidorFormDialogComponent
    ]
})

export class CompetidoresModule
{
}
