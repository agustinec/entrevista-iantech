import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';

import { Competidor } from 'app/shared/models/Competidor.model';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../i18n/en';
import { locale as spanish } from '../i18n/es';
import { MarcasService } from 'app/shared/services/marcas.service';
import { Marca } from 'app/shared/models/Marca.model';
import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';
import { ZonaDePrecios } from 'app/shared/models/ZonaDePrecios.model';
import { CompetidoresService } from 'app/shared/services/competidores.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from 'app/shared/components/error-dialog/error-dialog.component';

@Component({
    selector     : 'competidors-competidor-form-dialog',
    templateUrl  : './competidor-form.component.html',
    styleUrls    : ['./competidor-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CompetidoresCompetidorFormDialogComponent
{
    action: string;
    competidor: Competidor;
    marcas: Marca[];
    zonaDePrecios: ZonaDePrecios[];
    competidorForm: FormGroup;
    dialogTitle: string;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    errorDialogRef: MatDialogRef<ErrorDialogComponent>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CompetidoresCompetidorFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CompetidoresCompetidorFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _marcasService: MarcasService,
        private _zonaDePreciosService: ZonaDePreciosService,
        private _competidoresService: CompetidoresService,
        public errorDialog: MatDialog,
        public confirmDialog: MatDialog,
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        // Set the defaults
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Competidor';
            this.competidor = _data.competidor;
        }
        else
        {
            this.dialogTitle = 'New Competidor';
            this.competidor = new Competidor({});
        }

        this._marcasService.GetAllWitouthFilter()
        .then(e=> {
            this.marcas = e;
        });


        this._zonaDePreciosService.GetAllWitouthFilter()
        .then(e => {
            this.zonaDePrecios = e;
        })
        
        this.competidorForm = this.createCompetidorForm();
    }

    addCompetidor(formData)
    {
        console.log("va");
        this._competidoresService.addItem(formData.getRawValue())
        .then(resolved => {
            this.matDialogRef.close();
        },
        rejected => {
            if(rejected.status == 400)
            {
                this.showError("Los valores ingresados son incorrectos");
            }else if(rejected.status == 409)
            {
                this.showError(rejected.Conflict);
            }else
            {
                this.showError("Todos los campos son Necesarios.");
            }
        });
        
    }

    updateCompetidor(formData)
    {
        console.log("va");
        this._competidoresService.updateItem(formData.getRawValue())
        .then(
            resolved => {
                this.matDialogRef.close();
            },
            rejected => {
                if(rejected.status == 400)
                {
                    this.showError("Los valores ingresados son incorrectos");
                }else if(rejected.status == 409)
                {
                    this.showError(rejected.Conflict);
                }else
                {
                    this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                }
            });

        
        
    }

    deleteCompetidor(formData)
    {
        this.confirmDialogRef = this.confirmDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Está seguro que desea eliminar el Item?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._competidoresService.deleteItem(this.competidor)
                .then(resolved => {
                    this.matDialogRef.close();
                },
                rejected => {
                    if(rejected.status == 400)
                    {
                        this.showError("Los valores ingresados son incorrectos");
                    }else if(rejected.status == 409)
                    {
                        this.showError(rejected.Conflict);
                    }else
                    {
                        this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }


    showError(error)
    {
        this.errorDialogRef = this.errorDialog.open(ErrorDialogComponent, {
            disableClose: false
        });

        this.errorDialogRef.componentInstance.errorMessage = error;

        this.errorDialogRef.afterClosed().subscribe(result => {
            this.errorDialogRef = null;
        });
    }

    showErrors(errors)
    {
        this.errorDialogRef = this.errorDialog.open(ErrorDialogComponent, {
            disableClose: false
        });

        this.errorDialogRef.componentInstance.errors = errors;

        this.errorDialogRef.afterClosed().subscribe(result => {
            this.errorDialogRef = null;
        });
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create competidor form
     *
     * @returns {FormGroup}
     */
    createCompetidorForm(): FormGroup
    {
        return this._formBuilder.group({
            id      : [this.competidor.id],
            codigo    : [this.competidor.codigo],
            nombre: [this.competidor.nombre],
            calle  : [this.competidor.calle],
            latitud: [this.competidor.latitud],
            longitud : [this.competidor.longitud],
            marcador: [this.competidor.marcador],
            observar   : [this.competidor.observar],
            marcaId   : [this.competidor.marcaId],
            zonaDePrecioId : [this.competidor.zonaDePrecioId],
            createdAt      : [this.competidor.createdAt],
            updatedAt      : [this.competidor.updatedAt],
        });
    }
}
