export const locale = {
    lang: 'es',
    data: {
        'MARCAS': {
            'TITTLE': 'Lista de Marcas',
            'CODIGO': 'Código',
            'NOMBRE': 'Nombre',
            'SEARCH': 'Buscar marca...',
            'ELIMINAR': 'Eliminar',
            'AGREGAR': 'Agregar',
            'GUARDAR': 'Guardar'
        }
    }
};
