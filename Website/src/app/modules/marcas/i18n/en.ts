export const locale = {
    lang: 'en',
    data: {
        'MARCAS': {
            'TITTLE': 'Brands List', 
            'CODIGO': 'Code',
            'NOMBRE': 'Name',
            'SEARCH': 'Search brand...',
            'ELIMINAR': 'Delete',
            'AGREGAR': 'Add',
            'GUARDAR': 'Save'
        }
    }
};
