import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';

import { Marca } from 'app/shared/models/Marca.model';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../i18n/en';
import { locale as spanish } from '../i18n/es';
import { MarcasService } from 'app/shared/services/marcas.service';
import { ErrorDialogComponent } from 'app/shared/components/error-dialog/error-dialog.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector     : 'marcas-marca-form-dialog',
    templateUrl  : './form.component.html',
    styleUrls    : ['./form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class MarcasMarcaFormDialogComponent
{
    action: string;
    marca: Marca;
    marcaForm: FormGroup;
    dialogTitle: string;
    
    errorDialogRef: MatDialogRef<ErrorDialogComponent>;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<MarcasMarcaFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<MarcasMarcaFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _marcasService: MarcasService,
        public errorDialog: MatDialog,
        public confirmDialog: MatDialog,
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        // Set the defaults
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Marca';
            this.marca = _data.marca;
        }
        else
        {
            this.dialogTitle = 'New Marca';
            this.marca = new Marca({});
        }

        this.marcaForm = this.createMarcaForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create marca form
     *
     * @returns {FormGroup}
     */
    createMarcaForm(): FormGroup
    {
        return this._formBuilder.group({
            id      : [this.marca.id],
            codigo    : [this.marca.codigo],
            nombre: [this.marca.nombre],
            createdAt      : [this.marca.createdAt],
            updatedAt      : [this.marca.updatedAt],
        });
    }

    addItem(formData)
    {
        this._marcasService.addItem(formData.getRawValue())
        .then(resolved => {
            this.matDialogRef.close();
        },
        rejected => {
            if(rejected.status == 400)
            {
                this.showError("Los valores ingresados son incorrectos");
            }else if(rejected.status == 409)
            {
                this.showError(rejected.Conflict);
            }else
            {
                this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
            }
        });
        
    }

    updateItem(formData)
    {
        this._marcasService.updateItem(formData.getRawValue())
        .then(
            resolved => {
                this.matDialogRef.close();
            },
            rejected => {
                if(rejected.status == 400)
                {
                    this.showError("Los valores ingresados son incorrectos");
                }else if(rejected.status == 409)
                {
                    this.showError(rejected.Conflict);
                }else
                {
                    this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                }
            });

        
        
    }

    deleteItem(formData)
    {
        this.confirmDialogRef = this.confirmDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Está seguro que desea eliminar el Item?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._marcasService.deleteItem(this.marca)
                .then(resolved => {
                    this.matDialogRef.close();
                },
                rejected => {
                    if(rejected.status == 400)
                    {
                        this.showError("Los valores ingresados son incorrectos");
                    }else if(rejected.status == 409)
                    {
                        this.showError("El item contiene un Competidor asociado y no puede ser eliminado.");
                    }else
                    {
                        this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }
    
    
    showError(error)
    {
        this.errorDialogRef = this.errorDialog.open(ErrorDialogComponent, {
            disableClose: false
        });

        this.errorDialogRef.componentInstance.errorMessage = error;

        this.errorDialogRef.afterClosed().subscribe(result => {
            this.errorDialogRef = null;
        });
    }
}
