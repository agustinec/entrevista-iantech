import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { MarcasComponent } from './marcas.component';
import { MarcasService } from 'app/shared/services/marcas.service';
import { MarcasMarcaListComponent } from './list/list.component';
import { MarcasSelectedBarComponent } from './selected-bar/selected-bar.component';
import { MarcasMarcaFormDialogComponent } from './form/form.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FuseSidebarModule, FuseConfirmDialogModule } from '@fuse/components';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';

const routes = [
    {
        path     : 'marcas',
        component: MarcasComponent,
        resolve  : {
            marcas: MarcasService
        }
    }
];

@NgModule({
    declarations: [
        MarcasComponent,
        MarcasMarcaListComponent,
        MarcasSelectedBarComponent,
        MarcasMarcaFormDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule
    ],
    providers      : [
        MarcasService
    ],
    exports     : [
        MarcasComponent
    ],
    entryComponents: [
        MarcasMarcaFormDialogComponent
    ]
})

export class MarcasModule
{
}
