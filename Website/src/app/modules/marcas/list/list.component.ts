import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataSource } from '@angular/cdk/collections';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { MarcasService } from 'app/shared/services/marcas.service';

import { MarcasMarcaFormDialogComponent } from '../form/form.component';

@Component({
    selector     : 'marcas-marca-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class MarcasMarcaListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    marcas: any;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'Codigo', 'Nombre',  '_controls'];
    selectedMarcas: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MarcasService} _marcasService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _marcasService: MarcasService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._marcasService);

        this._marcasService.onChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(marcas => {
                this.marcas = marcas;
                console.log(this.marcas);
                this.checkboxes = {};
                marcas.map(marca => {
                    this.checkboxes[marca.id] = false;
                });
            });

        this._marcasService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedMarcas => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedMarcas.includes(id);
                }
                this.selectedMarcas = selectedMarcas;
            });

      

        this._marcasService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._marcasService.deselectItems();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit marca
     *
     * @param marca
     */
    editMarca(marca): void
    {
        this.dialogRef = this._matDialog.open(MarcasMarcaFormDialogComponent, {
            panelClass: 'marca-form-dialog',
            data      : {
                marca: marca,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this._marcasService.updateItem(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteMarca(marca);

                        break;
                }
            });
    }

    /**
     * Delete Marca
     */
    deleteMarca(marca): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._marcasService.deleteItem(marca);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param marcaId
     */
    onSelectedChange(marcaId): void
    {
        this._marcasService.toggleSelectedItem(marcaId);
    }

    
}

export class FilesDataSource extends DataSource<any>
{
    /**
     * Constructor
     *
     * @param {MarcasService} _marcasService
     */
    constructor(
        private _marcasService: MarcasService
    )
    {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]>
    {
        return this._marcasService.onChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void
    {
    }
}
