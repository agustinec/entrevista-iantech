import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';

@Component({
    selector   : 'selected-bar',
    templateUrl: './selected-bar.component.html',
    styleUrls  : ['./selected-bar.component.scss']
})
export class ZonaDePreciosSelectedBarComponent implements OnInit, OnDestroy
{
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    hasSelectedZonaDePrecios: boolean;
    isIndeterminate: boolean;
    selectedZonaDePrecios: string[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ZonaDePreciosService} _zonaDePrecioService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _zonaDePrecioService: ZonaDePreciosService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._zonaDePrecioService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedZonaDePrecios => {
                this.selectedZonaDePrecios = selectedZonaDePrecios;
                setTimeout(() => {
                    this.hasSelectedZonaDePrecios = selectedZonaDePrecios.length > 0;
                    this.isIndeterminate = (selectedZonaDePrecios.length !== this._zonaDePrecioService._data.length && selectedZonaDePrecios.length > 0);
                }, 0);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Select all
     */
    selectAll(): void
    {
        this._zonaDePrecioService.selectItem();
    }

    /**
     * Deselect all
     */
    deselectAll(): void
    {
        this._zonaDePrecioService.deselectItems();
    }

    /**
     * Delete selected zonaDePrecio
     */
    deleteSelectedZonaDePrecios(): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected zonaDePrecio?';

        this.confirmDialogRef.afterClosed()
            .subscribe(result => {
                if ( result )
                {
                    this._zonaDePrecioService.deleteSelectedItems();
                }
                this.confirmDialogRef = null;
            });
    }
}
