import { Component, ViewEncapsulation } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as spanish } from './i18n/es';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ZonaDePreciosZonaDePrecioFormDialogComponent } from './form/form.component';
import { fuseAnimations } from '@fuse/animations';



@Component({
    selector   : 'zonaDePrecios',
    templateUrl: './zonaDePrecios.component.html',
    styleUrls  : ['./zonaDePrecios.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ZonaDePreciosComponent
{
    

    dialogRef: any;
    hasSelectedZonaDePrecios: boolean;
    searchInput: FormControl;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ZonaDePreciosService} _zonaDePrecioService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _zonaDePrecioService: ZonaDePreciosService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);

        // Set the defaults
        this.searchInput = new FormControl('');

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._zonaDePrecioService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedZonaDePrecios => {
                this.hasSelectedZonaDePrecios = selectedZonaDePrecios.length > 0;
            });

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._zonaDePrecioService.onSearchTextChanged.next(searchText);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * New zonaDePrecio
     */
    newZonaDePrecio(): void
    {
        this.dialogRef = this._matDialog.open(ZonaDePreciosZonaDePrecioFormDialogComponent, {
            panelClass: 'zonaDePrecio-form-dialog',
            data      : {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response )
                {
                    return;
                }

                this._zonaDePrecioService.addItem(response.getRawValue());
            });
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

}
