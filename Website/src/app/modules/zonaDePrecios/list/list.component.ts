import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataSource } from '@angular/cdk/collections';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';

import { ZonaDePreciosZonaDePrecioFormDialogComponent } from '../form/form.component';

@Component({
    selector     : 'zonaDePrecio-zonaDePrecio-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ZonaDePreciosZonaDePreciosListComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    zonaDePrecio: any;
    dataSource: FilesDataSource | null;
    displayedColumns = ['checkbox', 'Codigo', 'Nombre',  '_controls'];
    selectedZonaDePrecios: any[];
    checkboxes: {};
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ZonaDePreciosService} _zonaDePrecioService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _zonaDePrecioService: ZonaDePreciosService,
        public _matDialog: MatDialog
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.dataSource = new FilesDataSource(this._zonaDePrecioService);

        this._zonaDePrecioService.onChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(zonaDePrecio => {
                this.zonaDePrecio = zonaDePrecio;
                console.log(this.zonaDePrecio);
                this.checkboxes = {};
                zonaDePrecio.map(zonaDePrecio => {
                    this.checkboxes[zonaDePrecio.id] = false;
                });
            });

        this._zonaDePrecioService.onSelectedChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selectedZonaDePrecios => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedZonaDePrecios.includes(id);
                }
                this.selectedZonaDePrecios = selectedZonaDePrecios;
            });

      

        this._zonaDePrecioService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this._zonaDePrecioService.deselectItems();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit zonaDePrecio
     *
     * @param zonaDePrecio
     */
    editZonaDePrecio(zonaDePrecio): void
    {
        this.dialogRef = this._matDialog.open(ZonaDePreciosZonaDePrecioFormDialogComponent, {
            panelClass: 'zonaDePrecio-form-dialog',
            data      : {
                zonaDePrecio: zonaDePrecio,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch ( actionType )
                {
                    /**
                     * Save
                     */
                    case 'save':

                        this._zonaDePrecioService.updateItem(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteZonaDePrecio(zonaDePrecio);

                        break;
                }
            });
    }

    /**
     * Delete ZonaDePrecio
     */
    deleteZonaDePrecio(zonaDePrecio): void
    {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._zonaDePrecioService.deleteItem(zonaDePrecio);
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * On selected change
     *
     * @param marcaId
     */
    onSelectedChange(marcaId): void
    {
        this._zonaDePrecioService.toggleSelectedItem(marcaId);
    }

    
}

export class FilesDataSource extends DataSource<any>
{
    /**
     * Constructor
     *
     * @param {ZonaDePreciosService} _zonaDePrecioService
     */
    constructor(
        private _zonaDePrecioService: ZonaDePreciosService
    )
    {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]>
    {
        return this._zonaDePrecioService.onChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void
    {
    }
}
