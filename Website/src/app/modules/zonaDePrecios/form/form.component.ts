import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';

import { ZonaDePrecios } from 'app/shared/models/ZonaDePrecios.model';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../i18n/en';
import { locale as spanish } from '../i18n/es';
import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';
import { ErrorDialogComponent } from 'app/shared/components/error-dialog/error-dialog.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector     : 'zonaDePrecio-zonaDePrecio-form-dialog',
    templateUrl  : './form.component.html',
    styleUrls    : ['./form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ZonaDePreciosZonaDePrecioFormDialogComponent
{
    action: string;
    zonaDePrecio: ZonaDePrecios;
    zonaDePreciosForm: FormGroup;
    dialogTitle: string;
    
    errorDialogRef: MatDialogRef<ErrorDialogComponent>;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<ZonaDePreciosZonaDePrecioFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ZonaDePreciosZonaDePrecioFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        public errorDialog: MatDialog,
        public confirmDialog: MatDialog,
        private _zonaDePreciosService:ZonaDePreciosService
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, spanish);
        // Set the defaults
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit ZonaDePrecio';
            this.zonaDePrecio = _data.zonaDePrecio;
        }
        else
        {
            this.dialogTitle = 'Nueva Zona de Precio';
            this.zonaDePrecio = new ZonaDePrecios({});
        }

        this.zonaDePreciosForm = this.createZonaDePrecioForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create zonaDePrecio form
     *
     * @returns {FormGroup}
     */
    createZonaDePrecioForm(): FormGroup
    {
        return this._formBuilder.group({
            id      : [this.zonaDePrecio.id],
            codigo    : [this.zonaDePrecio.codigo],
            nombre: [this.zonaDePrecio.nombre],
            createdAt      : [this.zonaDePrecio.createdAt],
            updatedAt      : [this.zonaDePrecio.updatedAt],
        });
    }

    addItem(formData)
    {
        this._zonaDePreciosService.addItem(formData.getRawValue())
        .then(resolved => {
            this.matDialogRef.close();
        },
        rejected => {
            if(rejected.status == 400)
            {
                this.showError("Los valores ingresados son incorrectos");
            }else if(rejected.status == 409)
            {
                this.showError(rejected.Conflict);
            }else
            {
                this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
            }
        });
        
    }

    updateItem(formData)
    {
        this._zonaDePreciosService.updateItem(formData.getRawValue())
        .then(
            resolved => {
                this.matDialogRef.close();
            },
            rejected => {
                if(rejected.status == 400)
                {
                    this.showError("Los valores ingresados son incorrectos");
                }else if(rejected.status == 409)
                {
                    this.showError(rejected.Conflict);
                }else
                {
                    this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                }
            });

        
        
    }

    deleteItem(formData)
    {
        this.confirmDialogRef = this.confirmDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = '¿Está seguro que desea eliminar el Item?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this._zonaDePreciosService.deleteItem(this.zonaDePrecio)
                .then(resolved => {
                    this.matDialogRef.close();
                },
                rejected => {
                    if(rejected.status == 400)
                    {
                        this.showError("Los valores ingresados son incorrectos");
                    }else if(rejected.status == 409)
                    {
                        this.showError("El item contiene un Competidor asociado y no puede ser eliminado.");
                    }else
                    {
                        this.showError("Ocurrió un error en el servidor, inténtelo nuevamente.");
                    }
                });
            }
            this.confirmDialogRef = null;
        });
    }
    
    
    showError(error)
    {
        this.errorDialogRef = this.errorDialog.open(ErrorDialogComponent, {
            disableClose: false
        });

        this.errorDialogRef.componentInstance.errorMessage = error;

        this.errorDialogRef.afterClosed().subscribe(result => {
            this.errorDialogRef = null;
        });
    }
}
