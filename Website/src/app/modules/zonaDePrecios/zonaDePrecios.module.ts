import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { ZonaDePreciosComponent } from './zonaDePrecios.component';
import { ZonaDePreciosService } from 'app/shared/services/zonaDePrecios.service';
import { ZonaDePreciosZonaDePreciosListComponent } from './list/list.component';
import { ZonaDePreciosSelectedBarComponent } from './selected-bar/selected-bar.component';
import { ZonaDePreciosZonaDePrecioFormDialogComponent } from './form/form.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FuseSidebarModule, FuseConfirmDialogModule } from '@fuse/components';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';

const routes = [
    {
        path     : 'zonadeprecios',
        component: ZonaDePreciosComponent,
        resolve  : {
            zonaDePrecio: ZonaDePreciosService
        }
    }
];

@NgModule({
    declarations: [
        ZonaDePreciosComponent,
        ZonaDePreciosZonaDePreciosListComponent,
        ZonaDePreciosSelectedBarComponent,
        ZonaDePreciosZonaDePrecioFormDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatTableModule,
        MatToolbarModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule
    ],
    providers      : [
        ZonaDePreciosService
    ],
    exports     : [
        ZonaDePreciosComponent
    ],
    entryComponents: [
        ZonaDePreciosZonaDePrecioFormDialogComponent
    ]
})

export class ZonaDePreciosModule
{
}
