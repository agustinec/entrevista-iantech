export const locale = {
    lang: 'en',
    data: {
        'ZONADEPRECIOS': {
            'TITTLE': 'Prices Zone List', 
            'CODIGO': 'Code',
            'NOMBRE': 'Name',
            'SEARCH': 'Search prices zone...',
            'ELIMINAR': 'Delete',
            'AGREGAR': 'Add',
            'GUARDAR': 'Save'
        }
    }
};
