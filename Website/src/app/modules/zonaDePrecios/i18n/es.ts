export const locale = {
    lang: 'es',
    data: {
        'ZONADEPRECIOS': {
            'TITTLE': 'Lista de Zonas de Precios',
            'CODIGO': 'Código',
            'NOMBRE': 'Nombre',
            'SEARCH': 'Buscar zona de precios...',
            'ELIMINAR': 'Eliminar',
            'AGREGAR': 'Agregar',
            'GUARDAR': 'Guardar'
        }
    }
};
