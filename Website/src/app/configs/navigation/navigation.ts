import { MainNavigation } from '@fuse/types';

export const navigation: MainNavigation[] = [
    {
        id       : 'competidores',
        title    : 'Competidores',
        translate: 'NAV.COMPETIDORES',
        type     : 'group',
        children : [
            {
                id       : 'competidores',
                title    : 'Competidores',
                translate: 'NAV.COMPETIDORES_LIST.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/competidores'
            },
            {
                id       : 'marcas',
                title    : 'Marcas',
                translate: 'NAV.MARCAS_LIST.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/marcas'
            },
            {
                id       : 'zonaDePrecios',
                title    : 'Zona de Precios',
                translate: 'NAV.MZONADEPRECIOS_LIST.TITLE',
                type     : 'item',
                icon     : 'email',
                url      : '/zonadeprecios'
            }
        ]
    }
];
