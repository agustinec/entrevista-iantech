export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'COMPETIDORES': 'Competitors',
            'COMPETIDORES_LIST'        : {
                'TITLE': 'Competitors list'
            },
            'MARCAS_LIST'        : {
                'TITLE': 'Brand list'
            },
            'MZONADEPRECIOS_LIST'        : {
                'TITLE': 'Price Zone List'
            }
        }
    }
};
