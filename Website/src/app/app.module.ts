import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { ThemeConfig } from 'app/configs/theme-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/shared/layouts/layout.module';
import { CompetidoresModule } from 'app/modules/competidores/competidores.module';
import { MarcasModule } from './modules/marcas/marcas.module';
import { ZonaDePreciosModule } from './modules/zonaDePrecios/zonaDePrecios.module';
import { ErrorDialogModule } from './shared/components/error-dialog/error-dialog.module';

const appRoutes: Routes = [
    {
        path      : '**',
        redirectTo: 'competidores'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(ThemeConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        ErrorDialogModule,

        // App modules
        LayoutModule,
        CompetidoresModule,
        MarcasModule,
        ZonaDePreciosModule
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
