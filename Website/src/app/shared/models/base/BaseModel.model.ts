
export class BaseModel
{
    id?: number;
    guid?: string;
    createdAt?: Date;
    updatedAt?: Date;

    /**
     * Constructor
     *
     * @param contact
     */
    constructor()
    {
    }
}
