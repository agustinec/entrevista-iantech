import { BaseModel } from './base/BaseModel.model';

export class ZonaDePrecios extends BaseModel
{
    codigo: string;
    nombre: string;

    /**
     * Constructor
     *
     * @param zonaDePrecios
     */
    constructor(zonaDePrecios)
    {
        super();
        {
            this.updatedAt = zonaDePrecios.updatedAt || null;
            this.createdAt = zonaDePrecios.createdAt || null;
            this.id = zonaDePrecios.id || null;
            this.codigo = zonaDePrecios.codigo || '';
            this.nombre = zonaDePrecios.nombre || '';
        }
    }
}
