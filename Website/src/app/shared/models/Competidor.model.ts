import { BaseModel } from './base/BaseModel.model';

export class Competidor extends BaseModel
{
    codigo: string;
    nombre: string;
    calle: string;
    latitud: number;
    longitud: number;
    marcador: boolean;
    observar: boolean;
    marcaId: number;
    zonaDePrecioId: number;

    /**
     * Constructor
     *
     * @param competidor
     */
    constructor(competidor)
    {
        super();
        {
            this.updatedAt = competidor.updatedAt || null;
            this.createdAt = competidor.createdAt || null;
            this.id = competidor.id || null;
            this.codigo = competidor.codigo || '';
            this.nombre = competidor.nombre || '';
            this.calle = competidor.calle || '';
            this.latitud = competidor.latitud || 0;
            this.longitud = competidor.longitud || 0;
            this.marcaId = competidor.marcaId || 0;
            this.zonaDePrecioId = competidor.zonaDePrecioId || 0;
            this.marcador = competidor.marcador || false;
            this.observar = competidor.observar || false;
        }
    }
}
