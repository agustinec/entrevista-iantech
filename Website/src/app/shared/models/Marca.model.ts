import { BaseModel } from './base/BaseModel.model';

export class Marca extends BaseModel
{
    codigo: string;
    nombre: string;

    /**
     * Constructor
     *
     * @param marca
     */
    constructor(marca)
    {
        super();
        {
            this.updatedAt = marca.updatedAt || null;
            this.createdAt = marca.createdAt || null;
            this.id = marca.id || null;
            this.codigo = marca.codigo || '';
            this.nombre = marca.nombre || '';
        }
    }
}
