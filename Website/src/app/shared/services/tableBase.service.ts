import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { isRejected } from 'q';

export class TableBaseService implements Resolve<any>
{
    onChanged: BehaviorSubject<any>;
    onSelectedChanged: BehaviorSubject<any>;
    onDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    _data: any[];
    _user: any;
    _selected: number[] = [];

    _searchText: string;
    _filterBy: string;


    constructor( private _httpClient: HttpClient, private _apiUrl:string )
    {
        this.onChanged = new BehaviorSubject([]);
        this.onSelectedChanged = new BehaviorSubject([]);
        this.onDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onFilterChanged = new Subject();
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.GetAlls()
            ]).then(
                ([files]) => {

                    this.onSearchTextChanged.subscribe(searchText => {
                        this._searchText = searchText;
                        this.GetAlls();
                    });

                    this.onFilterChanged.subscribe(filter => {
                        this._filterBy = filter;
                        this.GetAlls();
                    });

                    resolve();

                },
                reject
            );
        });
    }


    GetFilterNormalized()
    {
        if(this._searchText === undefined)
        {
            return "";
        }else
        {
            return encodeURIComponent(this._searchText);
        }
    }

    GetAlls(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get(this._apiUrl + '?filter=' + this.GetFilterNormalized())
                    .subscribe((response: any) => {

                        this._data = response;

                        this.onChanged.next(this._data);
                        resolve(this._data);
                    }, reject);
            }
        );
    }


    
    GetAllWitouthFilter(): Promise<any>
    {
        return new Promise((resolve, reject) => {
                this._httpClient.get(this._apiUrl)
                    .subscribe((response: any) => {

                        resolve(response);
                    }, reject);
            }
        );
    }
    // 

    /**
     * Toggle selected item by id
     *
     * @param id
     */
    toggleSelectedItem(id): void
    {
        if ( this._selected.length > 0 )
        {
            const index = this._selected.indexOf(id);

            if ( index !== -1 )
            {
                this._selected.splice(index, 1);

                this.onSelectedChanged.next(this._selected);

                return;
            }
        }

        this._selected.push(id);

        this.onSelectedChanged.next(this._selected);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll(): void
    {
        if ( this._selected.length > 0 )
        {
            this.deselectItems();
        }
        else
        {
            this.selectItem();
        }
    }

    /**
     * Select Item
     *
     * @param filterParameter
     * @param filterValue
     */
    selectItem(filterParameter?, filterValue?): void
    {
        this._selected = [];

        if ( filterParameter === undefined || filterValue === undefined )
        {
            this._selected = [];
            this._data.map(item => {
                this._selected.push(item.id);
            });
        }

        this.onSelectedChanged.next(this._selected);
    }

    /**
     * Deselect Item
     */
    deselectItems(): void
    {
        this._selected = [];

        this.onSelectedChanged.next(this._selected);
    }


     /**
     * Add Item
     *
     * @param item
     * @returns {Promise<any>}
     */
    addItem(item): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.post(this._apiUrl, {...item})
                .subscribe(response => {
                    this.GetAlls();
                    resolve(response);
                },
                error => {
                    reject(error);
                });
        });
    }

    
    /**
     * Update Item
     *
     * @param item
     * @returns {Promise<any>}
     */
    updateItem(item): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.put(this._apiUrl + "/" + item.id, {...item})
                .subscribe(
                response => {
                    this.GetAlls();
                    resolve(response);
                },
                error => {
                    reject(error);
                });
        });
    }


    /**
     * Delete Item
     *
     * @param item
     */
    deleteItem(item): Promise<any>
    {
        return new Promise((resolve, reject) => {

            this._httpClient.delete(this._apiUrl + "/" + item.id)
                .subscribe(
                response => {
                    const itemIndex = this._data.indexOf(item);
                    this._data.splice(itemIndex, 1);
                    this.onChanged.next(this._data);
                    resolve(response);
                },
                error => {
                    reject(error);
                });
        });

    }


    /**
     * Delete selected items
     */
    deleteSelectedItems(): void
    {
        for ( const id of this._selected )
        {
            const item = this._data.find(_item => {
                return _item.id === id;
            });
            
            this.deleteItem(item);
        }
        this.onChanged.next(this._data);
        this.deselectItems();
    }

}