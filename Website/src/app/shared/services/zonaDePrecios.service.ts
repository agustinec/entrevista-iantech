import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TableBaseService } from './tableBase.service';


@Injectable()
export class ZonaDePreciosService extends TableBaseService
{
    constructor(  _httpClient: HttpClient )
    {
        super(_httpClient, "http://localhost:3000/api/v1.0/ZonaDePrecios");
    }
}