import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { ErrorDialogComponent } from './error-dialog.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ErrorDialogComponent
    ],
    imports: [
        MatDialogModule,
        MatButtonModule,
        CommonModule
    ],
    entryComponents: [
        ErrorDialogComponent
    ],
})
export class ErrorDialogModule
{
}
