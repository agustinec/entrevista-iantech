import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector   : 'prisma-error-dialog',
    templateUrl: './error-dialog.component.html',
    styleUrls  : ['./error-dialog.component.scss']
})
export class ErrorDialogComponent
{
    public errors: [];
    
    public errorMessage: String ;
    /**
     * Constructor
     *
     * @param {MatDialogRef<ErrorDialogComponent>} dialogRef
     */
    constructor(
        public dialogRef: MatDialogRef<ErrorDialogComponent>
    )
    {
    }

}
