# Entrevista Iantech

### Tecnologías utilizadas:
- Api REST:
    - netcore 2.2.
    - WebApi C#
    - EF Core (Code First).
    - SQL Server.
- Website:
    - Angular 8.
    - Angular Material.


***
Estructura de Carpetas:

    .
    ├── Api
        ├── Prisma.Api               # Proyecto API REST en netcore 2.2
        |   ├── Areas                # Se usó el versionado de API's por área.
        |       ├── v1.0    
        |
        ├── Prisma.Api.Database      # Librería con las funciones de la base de datos
        |   ├── Contexts             # Contiene los Contexts de las base de datos.
        |   ├── Managers             # Managers de conexión.
        |   ├── Migrations           # Migraciones
        |   ├── ModelConfigurations  # Configuración de modelos, índices y relaciones.
        |
        ├── Prisma.Api.Models        # Librería con todos los modelos del proyecto.

    ├── WebSite                      # Frontend Angular 8
        ├── src
            ├── @fuse                # Theme utilizado 
            ├── app
                ├── configs          # Carpeta con configuraciones y navegación.
                ├── modules          # Lista de Módulos
                |   ├── competidores
                |   ├── marcas
                |   ├── zonaDePrecios
                ├── shared
                    ├── layouts      # layouts utilizados globalmente
                    ├── models       # modelos utilizados globalmente.
                    ├── services     # servicios y conexiones a la API.

***
## Ejecución con Docker

El docker-compose.yml incluido en el repositorio creará 3 contenedores:

#### 1.  db: 
   - Contiene una imagen de una base de datos MSSQL. Una vez iniciado, creará una carpeta llamada "mssql" en la raíz para poder persistir los datos en las siguientes compilaciones.
   - **Host**: localhost:1434
   - **Username**: sa
  - **Password**: Prisma2019


#### 2.  **prismaapi**:
  - Contiene la API REST que comunica el Website con la base de datos. Puede ingresar a la documentación cuando los contenedores están desplegados, visualizar y testear los servicios disponibles.
  - **Host**: http://localhost:3000/
  - **Documentación**: [http://localhost:3000/docs/](http://localhost:3000/docs/)
  - **Configuración**: La configuración de la conexión a la base de datos se encuentra en el archivo appsettings.json dentro de Prisma.Api. En caso de querer utilizar otra base de datos, diferente a la del contenedor, puede configurarla aquí mismo en la variable "PrismaDbConnectionString".


#### 3.  **prismaweb**:
  - Contiene el Frontend del desarrollo.
  - **Url:**: http://localhost:4200.

### Comandos:

```bash
# En la consola, ubicarse en la carpeta raíz donde Se encuentra el docker-compose.yml
cd CarpetaRaiz 

# En caso de que ya tenga los contenedores creados, debe forzar la re-creacion 
docker-compose up -d --force-recreate --build

# Iniciar los contenedores.
docker-compose up

```
